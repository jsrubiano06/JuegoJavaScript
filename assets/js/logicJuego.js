
console.log("*** AdivinaQuienSoy ***");

let personajes = [{
    name: 'carol || Capitana Marvel',
    foto: 'carol.png',
    preguntas: ['Es animal?', 'Es de marvel?', 'Es hombre?', 'Usa Armas?', 'tiene poderes?'],
    respuetas: ['no', 'si', 'no', 'no', 'si'],
}, {
    name: 'Goose || Gato ',
    foto: 'goose.png',
    preguntas: ['Vuela?', 'Es animal ?', 'Tiene poderes ?', 'Usa Armas?', 'Es ser humano?'],
    respuetas: ['no', 'si', 'no', 'no', 'no'],
}, {
    name: 'natasha || viuda negra',
    foto: 'natasha.png',
    preguntas: ['Es mujer?', 'Vuela ?', 'Tiene poderes ?', 'Usa Armas?', 'Es ser humano?'],
    respuetas: ['si', 'no', 'no', 'si', 'si'],
}, {
    name: 'steven || Capitan America',
    foto: 'steven.png',
    preguntas: ['Vuela?', 'Usa armadura ?', 'Tiene poderes ?', 'es hombre ?', 'Es ser humano?'],
    respuetas: ['no', 'si', 'no', 'si', 'si'],
}, {
    name: 'thor ',
    foto: 'thor.png',
    preguntas: ['Vuela?', 'Usa armadura ?', 'Tiene poderes ?', 'Usa Armas?', 'Es de este planeta?'],
    respuetas: ['si', 'si', 'si', 'si', 'no'],
}, {
    name: 'tony || Iron Man',
    foto: 'tony.png',
    preguntas: ['Vuela?', 'Usa armadura ?', 'Es hombre ?', 'Usa Armas?', 'Es ser humano?'],
    respuetas: ['si', 'si', 'si', 'si', 'si'],
}
];

let initpuntaje = 0;
let puntos = 0;
const btnJugar = document.getElementById("btnJugar");
const imgPersonaje = document.getElementById("imgPersonaje");
let indice = 0;
let opacidad = 20;

btnJugar.addEventListener('click', () => {

    const pregunta0 = document.getElementById("pregunta0");
    const pregunta1 = document.getElementById("pregunta1");
    const pregunta2 = document.getElementById("pregunta2");
    const pregunta3 = document.getElementById("pregunta3");
    const pregunta4 = document.getElementById("pregunta4");



    /* pregunta1 a la pregunta4 */

    imgPersonaje.src = "./assets/pic/" + personajes[indice].foto;
    imgPersonaje.style.filter = "blur(20px)";

    pregunta0.value = personajes[indice].preguntas[0];
    pregunta1.value = personajes[indice].preguntas[1];
    pregunta2.value = personajes[indice].preguntas[2];
    pregunta3.value = personajes[indice].preguntas[3];
    pregunta4.value = personajes[indice].preguntas[4];
    /* cargar el resto de pregutas */


})


// progar las respuestas recividas en el select

const rta0 = document.getElementById("rta0");

rta0.addEventListener('change', () => {

    if (rta0.value == personajes[indice].respuetas[0]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta0").src = "./assets/pic/si.png";

    } else {
        document.getElementById("icoRta0").src = "./assets/pic/no.png";
    }

    rta0.disabled = true;
})

const rta1 = document.getElementById("rta1");

rta1.addEventListener('change', () => {

    if (rta1.value == personajes[indice].respuetas[1]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta1").src = "./assets/pic/si.png";

    } else {
        document.getElementById("icoRta1").src = "./assets/pic/no.png";
    }

    rta1.disabled = true;
})


const rta2 = document.getElementById("rta2");

rta2.addEventListener('change', () => {

    if (rta2.value == personajes[indice].respuetas[2]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta2").src = "./assets/pic/si.png";

    } else {
        document.getElementById("icoRta2").src = "./assets/pic/no.png";
    }

    rta2.disabled = true;
})

const rta3 = document.getElementById("rta3");

rta3.addEventListener('change', () => {

    if (rta3.value == personajes[indice].respuetas[3]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta3").src = "./assets/pic/si.png";

    } else {
        document.getElementById("icoRta3").src = "./assets/pic/no.png";
    }

    rta3.disabled = true;
})

const rta4 = document.getElementById("rta4");

rta4.addEventListener('change', () => {

    if (rta4.value == personajes[indice].respuetas[4]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta4").src = "./assets/pic/si.png";

    } else {
        document.getElementById("icoRta4").src = "./assets/pic/no.png";
    }

    rta4.disabled = true;
})

function vaciarinput() {
    vaciar = document.getElementById("RespuestaGeneral");
    vaciar.value = '';
}
function vaciarpreguntas() {
    rta0.value = "";
    rta0.disabled = false;
    document.getElementById("icoRta0").src = "";
    rta1.value = "";
    rta1.disabled = false;
    document.getElementById("icoRta1").src = "";
    rta2.value = "";
    rta2.disabled = false;
    document.getElementById("icoRta2").src = "";
    rta3.value = "";
    rta3.disabled = false;
    document.getElementById("icoRta3").src = "";
    rta4.value = "";
    rta4.disabled = false;
    document.getElementById("icoRta4").src = "";
}


const btnRespuesta = document.getElementById("btnRespuesta");

btnRespuesta.addEventListener('click', () => {
    var longitud = personajes.length;
    aleatorio = Math.floor(Math.random() * (longitud))
    let random = []
    random.push(aleatorio);
    console.log(random);
    opacidad = 20;
    const RespuestaGeneral = document.getElementById("RespuestaGeneral").value;
    const error = document.getElementById("error");
    var pru = indice;
    personaje = personajes[pru].name;
    let similar = (personaje.toLowerCase()).includes(RespuestaGeneral.toLowerCase());
    console.log(similar);
    if (pru < longitud) {
        if (RespuestaGeneral == personajes[pru].name || similar == true) {
            vaciarinput();
            vaciarpreguntas();
            error.className = ' d-flex justify-content-center alert alert-success';
            error.innerHTML = '<img src="assets/pic/si.png" width="40px"> <div class="ml-2 mt-2">Felicidades</div>';
            pru = ++indice;
            if (pru < longitud) {
                imgPersonaje.src = "./assets/pic/" + personajes[pru].foto;
                imgPersonaje.style.filter = "blur(20px)";
                pregunta0.value = personajes[pru].preguntas[0];
                pregunta1.value = personajes[pru].preguntas[1];
                pregunta2.value = personajes[pru].preguntas[2];
                pregunta3.value = personajes[pru].preguntas[3];
                pregunta4.value = personajes[pru].preguntas[4];
                initpuntaje = initpuntaje + 20;
                console.log(initpuntaje)

            } else {
                initpuntaje = initpuntaje + 20;

                if (puntos > initpuntaje) {
                    alert('¡Casi Ganas Tu Puntaje es:'+ initpuntaje)
                } else {
                    alert('¡Excelente Tu Puntaje es:' + initpuntaje)
                }
            }

        } else {
            error.className = ' d-flex justify-content-center alert alert-danger';
            error.innerHTML = '<img src="assets/pic/no.png" width="40px"> <div class="ml-2 mt-2">ERROR! El nombre del personaje NO es correcto</div>';
            pru = ++indice;
            vaciarinput();
            vaciarpreguntas();
            puntos = puntos + 20;
            if (pru < longitud) {
                imgPersonaje.src = "./assets/pic/" + personajes[pru].foto;
                imgPersonaje.style.filter = "blur(20px)";
                pregunta0.value = personajes[pru].preguntas[0];
                pregunta1.value = personajes[pru].preguntas[1];
                pregunta2.value = personajes[pru].preguntas[2];
                pregunta3.value = personajes[pru].preguntas[3];
                pregunta4.value = personajes[pru].preguntas[4];
            } else {
                if (puntos > initpuntaje) {
                    alert('¡Perdiste Tu Puntaje es:'+ initpuntaje)
                } else {
                    alert('¡Excelente Tu Puntaje es:' + initpuntaje)
                }
            }

        }
    }
})